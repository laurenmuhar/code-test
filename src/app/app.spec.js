import app from './app';

describe('app', () => {

  describe('AppCtrl', () => {
    let ctrl, $httpBackend, scope, res;

    beforeEach(() => {
      angular.mock.module(app);

      angular.mock.inject(($controller, _$httpBackend_, _$rootScope_) => {
        $httpBackend = _$httpBackend_;
        scope = _$rootScope_;
        ctrl = $controller('AppCtrl', {$scope: scope});

        res = {
          'data': [{"name":"Bob","gender":"Male","age":23,"pets":[{"name":"Garfield","type":"Cat"},{"name":"Fido","type":"Dog"}]},{"name":"Jennifer","gender":"Female","age":18,"pets":[{"name":"Garfield","type":"Cat"}]},{"name":"Steve","gender":"Male","age":45,"pets":null},{"name":"Fred","gender":"Male","age":40,"pets":[{"name":"Tom","type":"Cat"},{"name":"Max","type":"Cat"},{"name":"Sam","type":"Dog"},{"name":"Jim","type":"Cat"}]},{"name":"Samantha","gender":"Female","age":40,"pets":[{"name":"Tabby","type":"Cat"}]},{"name":"Alice","gender":"Female","age":64,"pets":[{"name":"Simba","type":"Cat"},{"name":"Nemo","type":"Fish"}]}]
        }
        $httpBackend.when('JSONP','http://agl-developer-test.azurewebsites.net/people.json?callback=JSON_CALLBACK')
          .respond(200, res.data);
        scope.$digest();
      });
    });

    it('should contain the OwnerService', () => {
      expect(ctrl.OwnerService).toBeDefined();
    });

    it('should contain the initialized catList', () => {
      expect(ctrl.catList).toEqual([]);
    });

    it('should contain the initialized groupedBy', () => {
      expect(ctrl.groupedBy).toBeNull();
    });

    it('should initialized with values', () => {
      ctrl.$onInit();
      $httpBackend.flush();
      expect(ctrl.catList.length).toBe(7);
      expect(ctrl.groupedBy.Male.length).toBe(4);
      expect(ctrl.groupedBy.Female.length).toBe(3);
    });
  });
});
