'use strict';

import angular from 'angular';
import OwnerService from '../service/owner.module';
import filters from '../filter/filters';
import _ from 'lodash';
import '../style/app.css';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};


class AppCtrl {

  //Initialize constructor
  constructor(OwnerService) {
    this.OwnerService = OwnerService;
    this.catList = [];
    this.groupedBy = null;
  }

  $onInit() {
    //Call service to the list with values
    this.OwnerService.get().then(res => {
      //Iterate through each pet object and pop the items out into a list
      _.forEach(res, (owner) => {
        _.forEach(owner.pets, (pet) => {
          this.catList.push(pet);
        });
      });
      //Group the list of cats by gender and assign to the groupByList for the template
      this.groupedBy = _.groupBy(this.catList, 'ownerGender');
    });
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [OwnerService, filters])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;
