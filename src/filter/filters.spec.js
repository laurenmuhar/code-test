'use strict';

import filters from './filters';

describe('app', () => {

  describe('filters', () => {
    let scope, orderBy;

    beforeEach(() => {
      angular.mock.module('app.filters');

      angular.mock.inject((_$rootScope_, $filter) => {
        orderBy = $filter('orderBy');
        scope = _$rootScope_;
        scope.$digest();
      });
    });

    it('expect to be defined', () => {
        expect(orderBy).toBeDefined();
    });

    it('expect to order values correctly', () => {
        let data = [
          { 'name': 'Zod' },
          { 'name': 'Alex' },
          { 'name': 'Clark' },
          { 'name': 'Barry' }
        ];
        let result = orderBy(data, 'name');
        expect(result[0].name).toBe('Alex');
        expect(result[1].name).toBe('Barry');
        expect(result[2].name).toBe('Clark');
        expect(result[3].name).toBe('Zod');
    });
  });
});
