'use strict';

import angular from 'angular';

export default angular.module('app.filters', [])
  .filter('orderBy', () => {
    return function(data, val) {
      return _.orderBy(data, val);
    }
  })
  .name;
