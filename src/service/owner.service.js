'use strict';

import OwnerModel from './owner.model';

export function OwnerService($http, $sce) {
  'ngInject';

  let owner = {
    get() {
      //Set url of the api so the data can be retrieved
      const url = $sce.trustAsResourceUrl('http://agl-developer-test.azurewebsites.net/people.json');
      return $http.jsonp(url, {jsonpCallbackParam: 'callback'}).then(res => {
          return res.data.map((item) => {
            //Creates a new model that structures the data correctly
            return OwnerModel.fromApi(item);
          })
        }).catch(err => {
          return err;
        })
    }

  };

  return owner;
}
