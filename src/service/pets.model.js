'use strict';

class PetsModel {
  //Initialize values for constructor
  constructor(name = null, type = null, ownerGender = null) {
    this.name = name;
    this.type = type;
    this.ownerGender = ownerGender;
  }

  static fromApi(apiModel, gender) {
    /*Setup new pets model and assigning the owners gender to the pet model
    for easier sorting*/
    return new PetsModel(
      apiModel.name,
      apiModel.type,
      gender
    )
  }
}

export default PetsModel;
