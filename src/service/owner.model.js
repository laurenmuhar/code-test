'use strict';

import PetsModel from './pets.model';

class OwnerModel {
  //Initialize constructor with expected values
  constructor(pets = []){
    this.pets = pets;
  }

  static fromApi(apiModel) {
    //Filter out pets that aren't cats
    let pets = _.filter(apiModel.pets, (pet) => {
      return pet.type === 'Cat';
    });
    //create new pet objects that also inserts the owner gender into the pet model
    pets = pets.map(item => {
      return PetsModel.fromApi(item, apiModel.gender);
    })
    return new OwnerModel(
      pets
    )
  }
}

export default OwnerModel;
