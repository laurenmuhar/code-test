import OwnerService from './owner.service';

describe('app', () => {

  describe('OwnerService', () => {
    let scope, service, $httpBackend, res;

    beforeEach(() => {
      angular.mock.module('app.OwnerService');

      angular.mock.inject((_$httpBackend_, _OwnerService_, _$rootScope_) => {
        $httpBackend = _$httpBackend_;
        service = _OwnerService_;
        scope = _$rootScope_;

        res = {
          'data': [{"name":"Bob","gender":"Male","age":23,"pets":[{"name":"Garfield","type":"Cat"},{"name":"Fido","type":"Dog"}]},{"name":"Jennifer","gender":"Female","age":18,"pets":[{"name":"Garfield","type":"Cat"}]},{"name":"Steve","gender":"Male","age":45,"pets":null},{"name":"Fred","gender":"Male","age":40,"pets":[{"name":"Tom","type":"Cat"},{"name":"Max","type":"Cat"},{"name":"Sam","type":"Dog"},{"name":"Jim","type":"Cat"}]},{"name":"Samantha","gender":"Female","age":40,"pets":[{"name":"Tabby","type":"Cat"}]},{"name":"Alice","gender":"Female","age":64,"pets":[{"name":"Simba","type":"Cat"},{"name":"Nemo","type":"Fish"}]}]
        }
        $httpBackend.when('JSONP','http://agl-developer-test.azurewebsites.net/people.json?callback=JSON_CALLBACK')
          .respond(200, res.data);
        scope.$digest();
      });
    });

    it('should define the service', () => {
        expect(service).toBeDefined();
    });

    it('should get the expected data', () => {
        service.get().then(res => {
            expect(res.length).toBe(6);
            expect(res[0].pets[0].name).toBe('Garfield');
            expect(res[0].pets[0].ownerGender).toBe('Male');
            expect(res[0].pets[0].type).toBe('Cat');

            expect(res[3].pets.length).toBe(3);
            expect(res[3].pets[1].name).toBe('Max');
            expect(res[3].pets[1].ownerGender).toBe('Male');
            expect(res[3].pets[1].type).toBe('Cat');

            expect(res[5].pets.length).toBe(1);
            expect(res[5].pets[0].name).toBe('Simba');
            expect(res[5].pets[0].ownerGender).toBe('Female');
            expect(res[5].pets[0].type).toBe('Cat');
        });
        $httpBackend.flush();
    });
  });

  describe('OwnerService err response', () => {
    let scope, service, $httpBackend, res;

    beforeEach(() => {
      angular.mock.module('app.OwnerService');

      angular.mock.inject((_$httpBackend_, _OwnerService_, _$rootScope_) => {
        $httpBackend = _$httpBackend_;
        service = _OwnerService_;
        scope = _$rootScope_;

        $httpBackend.when('JSONP','http://agl-developer-test.azurewebsites.net/people.json?callback=JSON_CALLBACK')
          .respond(400, 'Request not found');
        scope.$digest();
      });
    });

    it('should get the expected data', () => {
        service.get().then(res => {
            expect(res.status).toBe(400);
            expect(res.data).toBe('Request not found');
        });
        $httpBackend.flush();
    });
  });
});
