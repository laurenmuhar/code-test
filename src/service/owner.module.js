'use strict';

import angular from 'angular';
import {OwnerService} from './owner.service';


export default angular.module('app.OwnerService', [])
  .factory('OwnerService', OwnerService)
  .name;
